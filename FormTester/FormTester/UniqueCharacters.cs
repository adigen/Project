﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FormTester
{
    public class UniqueCharacters
    {
        private string letters;
        public string newString;

        public UniqueCharacters(string letters, string newString)
        {
            this.letters = letters;
            this.newString = newString;
        }

         public void GetUniqueCharacters()
        {
            newString= String.Join("", letters.Distinct());
        }
    }
}
