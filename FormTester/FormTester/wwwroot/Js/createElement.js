﻿var data = {
    letters: "",
    newString: ""
};
function createElement(elementType, elementName)
{
    if (!document.getElementById('divId'))
    {
        var division = document.createElement("div");
        division.id = 'divId';
    }
    else
    {
        division = document.getElementById('divId');
    }
    box = document.createElement(elementType); //input element, text
    box.name = elementName;
    division.appendChild(box);
    document.body.appendChild(division);
}

function moveValue(data)
{
    data.letters = box.value;
}

function createButton(textName)
{
    if (!document.getElementById('divId'))
    {
        var division = document.createElement("div");
        division.id = 'divId';
    }
    else
    {
        division = document.getElementById('divId');
    }
        var btn = document.createElement('button'); //input element, Submit button
        var textNode = document.createTextNode(textName);
        btn.appendChild(textNode);

        btn.onclick = function () {
            moveValue(data);
            postRequest(data, 'newText');
        };
        
        division.appendChild(btn);
    
}
