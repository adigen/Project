﻿function postRequest(data, location) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            var newData = JSON.parse(this.responseText);
            document.getElementById(location).innerText += newData.newString + "\n";
        }
    };
    xhttp.open("Post", "api/values", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(data));
}   