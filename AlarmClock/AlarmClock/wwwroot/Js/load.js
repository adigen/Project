﻿function loadElements() {
    createElementjQuerry('body', '<div id=\'startPage\'></div>');
    createElementjQuerry('#startPage', '<p id =\'date\'></p>');
    createElementjQuerry('#startPage', '<p>Add alarm: </p>');
    createButonjQuerry('#startPage', 'addBtn', 'Add');
    createElementjQuerry('#startPage', '<p id =\'existentAlarm\'></p>');
    createElementjQuerry('body', '<div id=\'alarmPage\'></div>');
    createElementjQuerry('#alarmPage', '<p id=\'dateAlarm\'></p>');
    createElementjQuerry('#alarmPage', '<p id=\'daysOfTheWeek\'>Choose the day(s):</p>');
    createElementjQuerry('#alarmPage', '<select id=\'hour\'></select>');
    createElementjQuerry('#alarmPage', '<select id=\'minutes\'></select>');
    createElementjQuerry('#alarmPage', '<p id=\'label\'></p>');
    createElementjQuerry('body', '<div id=\'editPage\'></div>');
    createElementjQuerry('#editPage', '<p id=\'dateEdit\'></p>');
    createElementjQuerry('#editPage', '<p id=\'daysOfTheWeekEdit\'>Edit the day(s):</p>');
    createElementjQuerry('#editPage', '<select id=\'hourEdit\'></select>');
    createElementjQuerry('#editPage', '<select id=\'minutesEdit\'></select>');
    createElementjQuerry('#editPage', '<p id=\'labelEditLocation\'></p>');
    createElementjQuerry('#label', '<input id=\'text\' placeholder=\'Label for alarm\'/>');
    createElementjQuerry('#labelEditLocation', '<input id=\'labelEdit\' placeholder=\'Label for alarm\'/>')
    createButonjQuerry('#label', 'addAlarmBtn', 'Add alarm');
    createButonjQuerry('#labelEditLocation', 'editBtn', 'Edit');
    requestFromServer();
    weekDaysCheckbox('daysOfTheWeekEdit');
    populateHoursandMinutes('minutesEdit', 60);
    populateHoursandMinutes('hourEdit', 24);
    weekDaysCheckbox('daysOfTheWeek');
    populateHoursandMinutes('minutes', 60);
    populateHoursandMinutes('hour', 24);
}

function loadClickActions() {
    startPageToggleAddPage();
    startPageToggleEditPage();
    editBtnOnClick();
    addAlarmBtnOnClick();
}

function loadStyle() {
    backgrounds('startPage', '#00BFFF');
    backgrounds('alarmPage', '#8A2BE2');
    backgrounds('editPage', '#00FF7F');
    //deepskyblue color for startPage div
    //blueviolet color for alarmPage
    //spring green color for editPage
}