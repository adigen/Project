﻿var daysAlarmOn = {
    Monday: 1,
    Tuesday: 2,
    Wednesday: 3,
    Thursday: 4,
    Friday: 5,
    Saturday: 6,
    Sunday: 7
};

function alarmOn(conditionForDays, hour, minutes, label) {
    var day = "";
    verifyDayAlarm(conditionForDays[0], daysAlarmOn.Monday, hour, minutes, label);
    verifyDayAlarm(conditionForDays[1], daysAlarmOn.Tuesday, hour, minutes, label);
    verifyDayAlarm(conditionForDays[2], daysAlarmOn.Wednesday, hour, minutes, label);
    verifyDayAlarm(conditionForDays[3], daysAlarmOn.Thursday, hour, minutes, label);
    verifyDayAlarm(conditionForDays[4], daysAlarmOn.Friday, hour, minutes, label);
    verifyDayAlarm(conditionForDays[5], daysAlarmOn.Saturday, hour, minutes, label);
    verifyDayAlarm(conditionForDays[6], daysAlarmOn.Sunday, hour, minutes, label);    
}

function verifyDayAlarm(condition, dayAlarm, hour, minutes, label){
    if(condition == true){
        day = dayAlarm;
        checkDay(day, hour, minutes, label);
    }
}

function checkDay(day, hour, minutes, label) {
    let sound = new Audio("https://www.freespecialeffects.co.uk/soundfx/clocks/clock_chime.wav");
    let today = new Date();
    if ((today.getUTCDay() === day) && (today.getHours() === hour) && (today.getMinutes() === minutes)) {
        sound.loop = true;
        
        if (alert("Alarm on:" + label))
            sound.play();
        else
            sound.loop = false;
    }
}