﻿var data = {
    day: '',
    label: '',
    utcTime: ''
};

//enum with flags
var days = {
    Monday: 1,
    Tuesday: 2,
    Wednesday: 4,
    Thursday: 8,
    Friday: 16,
    Saturday: 32,
    Sunday: 64
};

var formatTime = {
        hour: '',
        minutes: ''
};

function moveValue(data, dayDivId, hourId, minutesId, labelId) {
    data.day = "";
    moveDays("0" + dayDivId, days.Monday);
    moveDays("1" + dayDivId, days.Tuesday);
    moveDays("2" + dayDivId, days.Wednesday);
    moveDays("3" + dayDivId, days.Thursday);
    moveDays("4" + dayDivId, days.Friday);
    moveDays("5" + dayDivId, days.Saturday);
    moveDays("6" + dayDivId, days.Sunday);

    moveTime(hourId, minutesId); 
    moveLabel(labelId);
}

function moveTime(hourId, minutesId)
{
    formatTime.hour = getHourAndMinutes(hourId); // to get exactly hour
    formatTime.minutes = getHourAndMinutes(minutesId); // to get exactly minute

    var time = new Date();

    time.setHours(formatTime.hour);
    time.setMinutes(formatTime.minutes);

    data.utcTime = time.toUTCString();    
}

function moveLabel(labelId) {
    data.label = $("#" + labelId).val();
}

function moveDays(id, dayToAdd)
{
    if ($("#"+id).is(":checked"))
        data.day |= dayToAdd;
}

function moveId(id) {
    data.id = id;
}

function getHourAndMinutes(id) {
    let time = $("#"+id).val(); 

    return time;
}

function daysWithAlarmOn(conditionForDays) {
    var daysArray = "";

    daysArray = daysActive(conditionForDays[0], 'Monday; ');
    daysArray += daysActive(conditionForDays[1], 'Tuesday; ');
    daysArray += daysActive(conditionForDays[2], 'Wednesday; ');
    daysArray += daysActive(conditionForDays[3], 'Thursday; ');
    daysArray += daysActive(conditionForDays[4], 'Friday; ');
    daysArray += daysActive(conditionForDays[5], 'Saturday; ');
    daysArray += daysActive(conditionForDays[6], 'Sunday; ');

    return daysArray;
}

function daysActive(conditionForDays, day) {
    let daysArray = '';
    if (conditionForDays)
         daysArray += day;
    return daysArray;
}

//convert a mask to an array of bools, to see which day is
function arrayFromMask(nMask) {
    // nMask must be between -2147483648 and 2147483647
    if (nMask > 0x7fffffff || nMask < -0x80000000) {
        throw new TypeError('arrayFromMask - out of range');
    }
    for (var nShifted = nMask, aFromMask = []; nShifted;
        aFromMask.push(Boolean(nShifted & 1)), nShifted >>>= 1);
    return aFromMask;
}

function checkboxDayActivated(conditionForDays) {
    //reset the previous days checked
    for (var i = 0; i < 7; i++)
        checkAndUncheck(i, 'daysOfTheWeekEdit', false);
    //check back the days who have an alarm
    for (var i = 0; i < 7; i++)
        if (conditionForDays[i])
            checkAndUncheck(i, 'daysOfTheWeekEdit', true);
}

function checkAndUncheck(index, idDiv, checkboxState) {
    var id = index + idDiv;
    $("#" + id).prop('checked', checkboxState);
}

function getDataForEditPage(response) {
    $("#labelEdit").val(response.label);

    var hoursAndMinutes = new Date(response.utcTime);

    $("#hourEdit option")[hoursAndMinutes.getHours()].selected = true;
    $("#minutesEdit option")[hoursAndMinutes.getMinutes()].selected = true;
}
