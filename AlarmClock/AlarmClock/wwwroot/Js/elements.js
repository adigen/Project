﻿function createElementjQuerry(location, element) {
    $(location).append(element);
}

function createButonjQuerry(location, btnId, btnValue) {
    let $but = $('<input/>').attr({ type: 'button', id: btnId, value: btnValue });

    $(location).append($but);
}

function createEditButton(idRequest, buttonId) {
    let id = 'edit' + buttonId;
    createButonjQuerry('#existentAlarm', id, 'Edit Alarm');
    editAlarmBtnOnClcik(id, idRequest);
}

function createDeleteButton(idRequest, buttonId) {
    let id = 'del' + buttonId;
    createButonjQuerry('#existentAlarm', id, 'Delete Alarm');
    deleteAlarmBtnOnClick(id, idRequest);
}