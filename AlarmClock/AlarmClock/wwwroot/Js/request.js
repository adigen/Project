﻿//detelete request for alarm
function deleteRequest(id) {
    $.ajax({
        type: 'DELETE',
        url: `api/values/${id}`,        
        success: function () {
            requestFromServer();
        }
    });
}

//post request which adds alarms
function postRequest(data) {
    $.ajax({
        type: "POST",
        url: 'api/values',
        contentType: 'application/json',
        data:  JSON.stringify(data) ,
        success: function () {
            requestFromServer();
        }
    });
}

//put request which edits alarms
function putRequest(data) {    
    $.ajax({
        type: "PUT",
        url: `api/values/${data.id}`,
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: function () {
            requestFromServer();
        }
    });
}

//jquery request for get
function requestFromServer() {
    $.get("api/values", function (data) {
        refreshAlarmsText(data);

        for (var i = 0; i < data.alarms.length; i++) {
            let conditionForDays = arrayFromMask(data.alarms[i].day);
            populateData(conditionForDays, data.alarms, i);
            displayText(data.alarms[i]);

            alarmOn(conditionForDays, hour, minutes, data.alarms[i].label);
        }
    }, "json");
}
const interval = setInterval(function () { requestFromServer(); }, 60000);

//get request from server by id, in order to edit the alarm
function getById(id) {
    $.ajax({
        type: 'GET',
        url: `api/values/${id}`,
        success: function (response) {
            //response = JSON.parse(this.response);
            let conditionForDays = arrayFromMask(response.day);
            checkboxDayActivated(conditionForDays);
            getDataForEditPage(response);
        }
    });
}