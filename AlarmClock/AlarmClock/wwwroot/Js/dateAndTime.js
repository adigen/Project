﻿//create the actual data and actual time to print
function dateAndTime() {
    let currentDate = new Date();
    let hours = addZeroToMinutesAndHours(currentDate.getHours());
    let minutes = addZeroToMinutesAndHours(currentDate.getMinutes());
    let seconds = addZeroToMinutesAndHours(currentDate.getSeconds());
    let date = addZeroToMinutesAndHours(currentDate.getDate());
    let month = addZeroToMinutesAndHours(currentDate.getMonth() + 1);
    let dateTime = date + "/" + month + "/" + currentDate.getFullYear() + " @ "
        + hours + ":" + minutes + ":" + seconds;

    timeRefresh('date', dateTime);
    timeRefresh('dateEdit', dateTime);
    timeRefresh('dateAlarm', dateTime);
    let time = setTimeout(dateAndTime, 1000);
}

function timeRefresh(id, dateTime) {
    $('#' + id).empty();
    $('#' + id).append("Curret date and time: " + dateTime);
}

function populateHoursandMinutes(locationId, limit) {
    for (let i = 0; i < limit; i++) {
        i = addZeroToMinutesAndHours(i);
        $('#' + locationId).append($("<option>").attr('value', i).text(i));
    }
}

function displayTime(timeToConvert) {
    let timeToDisplay = new Date(timeToConvert);
    let hour = addZeroToMinutesAndHours(timeToDisplay.getHours());
    let minutes = addZeroToMinutesAndHours(timeToDisplay.getMinutes());

    return hour + ':' + minutes;
}

function addZeroToMinutesAndHours(conditionToApply) {
    let time = conditionToApply < 10 ? "0" + conditionToApply : conditionToApply;
    return time;
}