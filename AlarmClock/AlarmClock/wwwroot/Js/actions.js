﻿function startPageToggleEditPage() {
    $("#editPage").hide();

    $(document).on("click", 'input[value=\'Edit Alarm\'], #editBtn', function () {
        $("#startPage, #editPage").toggle();
    });
}

function startPageToggleAddPage() {
    $("#alarmPage").hide();
    $(document).ready(function () {
        $("#addBtn, #addAlarmBtn").click(function () {
            $("#startPage, #alarmPage").toggle();
            $("#text").val(''); //label reset when press button
            $('input[type=checkbox]').each(function () {
                this.checked = false;
            }); //checkboxex reset when press button
            $("#hour").prop('selectedIndex', 0);
            $("#minutes").prop('selectedIndex', 0);
        });
    });
}

function editBtnOnClick(){
    $("#editBtn").click(function () {
        moveValue(data, 'daysOfTheWeekEdit', 'hourEdit', 'minutesEdit', 'labelEdit');
        if (data.day !== "")
            putRequest(data);
    });
}

function addAlarmBtnOnClick() {
    $('#addAlarmBtn').click(function () {
        moveValue(data, 'daysOfTheWeek', 'hour', 'minutes', 'text');
        if (data.day !== "")
            postRequest(data);
    });
}

function editAlarmBtnOnClcik(id, idRequest) {
    $('#'+id).click(function () {
        getById(idRequest);
        moveId(idRequest);
    });
}

function deleteAlarmBtnOnClick(id, idRequest) {
    $('#' + id).click(function () {
        deleteRequest(idRequest);
    });
}


//what's next is for requestFromServer 
var display = {
    days: '',
    time: ''
};

var data = {
    id: "00000000-0000-0000-0000-000000000000"
};

function refreshAlarmsText(data) {
    $("#existentAlarm").empty();
    $("#existentAlarm").append("Next Alarms: <br>");
    if (data.alarms.length === 0)
        $("#existentAlarm").append("No alarms for the moment.");
}

function displayText(data) {
    $("#existentAlarm")
        .append("<br>" + "Day: " + display.days)
        .append(", @ time: " + display.time)
        .append(", with label: " + data.label + '<br>');
}

function populateData(conditionForDays, dataFromServer, index) {
    //populate daysArray with every day to display on client    
    display.days = daysWithAlarmOn(conditionForDays);

    //final display                
    data.id = dataFromServer[index].guid;
    createDeleteButton(data.id, index);
    createEditButton(data.id, index);

    display.time = displayTime(dataFromServer[index].utcTime);
}

function weekDaysCheckbox(location) {

    var d_names = ["Monday", "Tuesday", "Wednesday",
        "Thursday", "Friday", "Saturday", "Sunday"];
    for (day in d_names) {
        let id = day + location;
        let checkbox = $(document.createElement('input')).attr({
            id: id,
            value: d_names[day],
            type: 'checkbox'
        })
        var label = $("<label>").html('<br>' + d_names[day]);

        label.append(checkbox);
        $("#" + location).append(label);
    }
}