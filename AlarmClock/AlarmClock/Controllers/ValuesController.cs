﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace AlarmClock.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<AlarmClock.Alarm> Get()
        {
            AlarmClock.AlarmClocko alarmList = new AlarmClock.AlarmClocko();
            var alarmsDeserialized = AlarmClock.SerializeDeserialize.LoadAlarm(alarmList);

            return alarmsDeserialized;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public AlarmClock.Alarm Get(string id)
        {
            AlarmClock.AlarmClocko alarmList = new AlarmClock.AlarmClocko();
            var alarmsDeserialized = AlarmClock.SerializeDeserialize.LoadAlarm(alarmList);

            var currentAlarm = alarmsDeserialized.GetAlarmById(id);
            return currentAlarm;
        }

        // POST api/values
        [HttpPost]
        public AlarmClock.Alarm Post([FromBody]AlarmClock.Alarm jsonAlarms)
        {
            jsonAlarms.Add();

            return jsonAlarms;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(string id, [FromBody]AlarmClock.Alarm newAlarm)
        {
            newAlarm.Edit(id);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            AlarmClock.AlarmClocko alarmList = new AlarmClock.AlarmClocko();
            var alarmsDeserialized = AlarmClock.SerializeDeserialize.LoadAlarm(alarmList);

            alarmsDeserialized.RemoveById(id);

            AlarmClock.SerializeDeserialize.SaveAlarm(alarmsDeserialized);
        }
    }
}
