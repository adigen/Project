﻿using System;
using System.Diagnostics;
using System.Globalization;

namespace AlarmClock.AlarmClock
{
#pragma warning disable CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()
    public class Alarm
#pragma warning restore CS0659 // Type overrides Object.Equals(object o) but does not override Object.GetHashCode()
    {
        public Days day;
        public string label; //name of the alarm
        public string guid;
        public DateTime utcTime;
        private AlarmClocko alarms;

        [Flags]
        public enum Days
        {
            Monday = 1,
            Tuesday = 2,
            Wednesday = 4,
            Thursday = 8,
            Friday = 16,
            Saturday = 32,
            Sunday = 64
        }

        public Alarm(DateTime utcTime, Days day, string label, string guid = "00000000-0000-0000-0000-000000000000")
        {
            this.utcTime = utcTime;
            this.day = day;
            this.label = label;
            this.guid = guid;
        }

        public bool CheckAlarmState(DateTime utcTime, Alarm[] clockAlarm, Days day, string label)
        {
            bool alarm = false;
            for (int i = 0; i < clockAlarm.Length; i++)
                alarm = (CheckConfig(utcTime, clockAlarm[i], day, label));

            return alarm;
        }

        public void Add()
        {
            OpenOrCreateFile();

            alarms.Add(utcTime, day, label);

            SerializeDeserialize.SaveAlarm(alarms);
        }

        private void OpenOrCreateFile()
        {
            try
            {
                AlarmClocko listWithAlarms = new AlarmClocko();
                alarms = SerializeDeserialize.LoadAlarm(listWithAlarms);
            }
            catch (System.IO.IOException e)
            {
                Debug.WriteLine("Error: {0}", e.Message);
            }
            finally
            {
                if (alarms == null)
                {
                    alarms = new AlarmClocko();
                }
            }
        }

        public void Edit(string id)
        {
            OpenOrCreateFile();

            alarms.Edit(utcTime, day, label, id);

            SerializeDeserialize.SaveAlarm(alarms);
        }

        private static bool CheckConfig(DateTime utcTime, Alarm clockAlarm, Days day, string label)
        {
            return clockAlarm.day.Equals(day) && clockAlarm.utcTime.Equals(utcTime) &&
                clockAlarm.label.Equals(label);
        }

        public override string ToString()
        {
            return utcTime + "/" + day + "/" + label;
        }

        public override bool Equals(object obj)
        {
            Alarm alarm = obj as Alarm;
            if (alarm == null)
                return false;

            return (utcTime == alarm.utcTime) && (day == alarm.day) && (label == alarm.label);
        }
    }
}