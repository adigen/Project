﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace AlarmClock.AlarmClock
{
    [Newtonsoft.Json.JsonObject]
    public class AlarmClocko : ICollection<Alarm>
    {
        private int count = 0;

        [Newtonsoft.Json.JsonIgnore]
        public int Count => count;

        [Newtonsoft.Json.JsonProperty("alarms")]
        public List<Alarm> Alarms { get; set; } = new List<Alarm>();

        [Newtonsoft.Json.JsonIgnore]
        public bool IsReadOnly => false;
        
        public void Add(DateTime utcTime, Alarm.Days days, string label)
        {
            var guid = Guid.NewGuid().ToString();

            Add(new Alarm(utcTime, days, label, guid));
        }

        public void Add(Alarm item)
        {
            Alarms.Add(item);
            count++;
        }

        public void Clear()
        {
            count = 0;
        }

        public bool Contains(Alarm item)
        {
            if (Alarms.Any(alarm => alarm.Equals(item)))
                 return true;
            return false;
        }

        public void CopyTo(Alarm[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool Remove(Alarm item)
        {
            if (Contains(item))
            {
                Alarms.RemoveAll(alarm => alarm.Equals(item));
                count--;
                return true;
            }
            return false;
        }

        public void RemoveById(string id)
        {
            Alarms.RemoveAll(alarm => alarm.guid == id);
        }

        public Alarm GetAlarmById(string id)
        {
            var alarm = Alarms.First(al => al.guid == id);
            
            return alarm;
        }

        public IEnumerator<Alarm> GetEnumerator()
        {
            foreach (var al in Alarms)
                yield return al;
        }

        public void Edit(DateTime utcTime, Alarm.Days day, string label, string id = "00000000-0000-0000-0000-000000000000")
        {
            var alarm = GetAlarmById(id);
            alarm.utcTime = utcTime;
            alarm.day = day;
            alarm.label = label;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}