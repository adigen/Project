﻿using Newtonsoft.Json;
using System.Diagnostics;
using System.IO;

namespace AlarmClock.AlarmClock
{
    public static class SerializeDeserialize
    {
        public static void SaveAlarm(AlarmClocko alarm)
        {
            var pathToFile = @"C:\Users\No\Desktop\AlarmFolder\alarm.txt";

            using (StreamWriter file = File.CreateText(pathToFile))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, alarm);
            }
        }

        public static AlarmClocko LoadAlarm(AlarmClocko alarm)
        {
            var pathToFile = @"C:\Users\No\Desktop\AlarmFolder\alarm.txt";
            try
            {
                using (StreamReader file = File.OpenText(pathToFile))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    alarm = (AlarmClocko)serializer.Deserialize(file, typeof(AlarmClocko));
                }
            }
            catch(FileNotFoundException e)
            {
                Debug.WriteLine("Error: {0}", e.Message);
            }

            return alarm;
        }
    }
}