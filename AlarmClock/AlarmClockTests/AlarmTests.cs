using AlarmClock.AlarmClock;
using System;
using Xunit;

namespace AlarmClockTests
{
    public class AlarmTests
    {
        [Fact]
        public void CheckIfActivated()
        {
            var time = DateTime.Now;
            var alarm = new Alarm[] { new Alarm(time, Alarm.Days.Wednesday, "Morning") };

            Assert.False(alarm[0].CheckAlarmState(time, alarm, Alarm.Days.Monday, "Morning"));
            Assert.True(alarm[0].CheckAlarmState(time, alarm, Alarm.Days.Wednesday, "Morning"));
        }
    }
}

