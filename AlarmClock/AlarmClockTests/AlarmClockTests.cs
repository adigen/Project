﻿using AlarmClock.AlarmClock;
using System;
using System.Linq;
using Xunit;

namespace AlarmClockTests
{
    public class AlarmClockTests
    {
        [Theory]
        [InlineData("2017-08-21T10:11:44Z", Alarm.Days.Monday | Alarm.Days.Thursday | Alarm.Days.Wednesday,
                "Morning")]
        public void AddAlarmsTest(DateTime time, Alarm.Days day, string label)
        {
            var alarmList = new AlarmClocko();
            alarmList.Add(new Alarm(time, day, label));
            Assert.Equal(new[] { new Alarm(time, day, label) },
                alarmList);
        }

        [Theory]
        [InlineData("2017-08-21T10:11:44Z", Alarm.Days.Monday, "Morning")]
        public void ContainsTest(DateTime time, Alarm.Days day, string label)
        {
            var alarmList = new AlarmClocko();
            alarmList.Add(time, day, label);
            Assert.True(alarmList.Contains(new Alarm(time, day, label)));
        }

        [Theory]
        [InlineData("2017-08-21T10:11:44Z", Alarm.Days.Monday, "Morning")]
        public void AddItemTest(DateTime time, Alarm.Days day, string label)
        {
            var alarm = new Alarm(time, day, label);
            var alarmList = new AlarmClocko();
            alarmList.Add(alarm);
            Assert.True(alarmList.Contains(alarm));
        }

        [Theory]
        [InlineData("2017-08-21T10:11:44Z", Alarm.Days.Monday, "Morning")]
        public void ClearTest(DateTime time, Alarm.Days day, string label)
        {
            var alarmList = new AlarmClocko();
            alarmList.Add(time, day, label);
            alarmList.Clear();
            Assert.Equal(0, alarmList.Count);
        }

        [Theory]
        [InlineData("2017-08-21T10:11:44Z", Alarm.Days.Friday, "Test")]
        [InlineData("2017-08-21T10:11:44Z", Alarm.Days.Monday | Alarm.Days.Friday, "Test")]
        public void RemoveTest(DateTime time, Alarm.Days day, string label)
        {
            var alarmList = new AlarmClocko();
            var alarm = new Alarm(time, day, label);
            alarmList.Add(alarm);
            alarmList.Remove(alarm);
            Assert.Empty(alarmList);
            Assert.Equal(0, alarmList.Count);
            Assert.False(alarmList.Contains(alarm));
        }

        [Theory]
        [InlineData("2017-08-21T10:11:44Z", Alarm.Days.Friday, "First")]
        public void RemoveByIdTest(DateTime time, Alarm.Days day, string label)
        {
            var alarmList = new AlarmClocko();
            var firstAlarm = new Alarm(time, day, label);
            alarmList.Add(firstAlarm);
            alarmList.RemoveById(firstAlarm.guid);
            Assert.False(alarmList.Contains(firstAlarm));
        }

        [Theory]
        [InlineData("2017 - 08 - 21T10: 11:44Z", Alarm.Days.Monday, "Alarm")]
        public void GetAlarmByIdTest(DateTime time, Alarm.Days day, string label)
        {
            var alarmList = new AlarmClocko();
            var firstAlarm = new Alarm(time, day, label);
            alarmList.Add(firstAlarm);
            Assert.Equal(alarmList.First(), alarmList.GetAlarmById(firstAlarm.guid));
        }

        [Theory]
        [InlineData("2017 - 08 - 21T10: 11:44Z", Alarm.Days.Monday, "Alarm")]
        public void EditTest(DateTime time, Alarm.Days day, string label)
        {
            var alarmList = new AlarmClocko();
            var firstAlarm = new Alarm(time, day, label);
            alarmList.Add(firstAlarm);
            alarmList.Edit(time, Alarm.Days.Friday, label);
            Assert.Equal(new[] { new Alarm(time, Alarm.Days.Friday, label) }, alarmList);
        }
    }
}
